module Main where

import Test.Tasty
import Test.Tasty.HUnit

import Lib


initTestSuite :: TestTree
initTestSuite = testGroup " Init tests"
                [ testCase "Hola" $ cadena "Hola"@?="Hola"]

main = defaultMain initTestSuite