module Main where

import Test.Tasty
import Test.Tasty.HUnit

import Introduction

pdfHaskellSuite :: TestTree
pdfHaskellSuite = testGroup " Pdf Haskell tests" $
                [ testGroup "Chapter 1 Introduction 1"$
                    [ testGroup "FACTORIAL" $
                        [ testGroup "factorial 1" $
                            [ testCase " fact1 4 -> 24" $ fact1 4 @?= 24
                            , testCase " fact1 5 -> 120" $ fact1 5 @?= 120]
                        , testGroup "factorial 2" $
                            [ testCase " fact2 3 -> 6" $ fact2 3 @?= 6
                            , testCase " fact2 6 -> 720" $ fact2 6 @?= 720]
                        , testGroup "factorial 3" $
                            [ testCase " fact3 2 -> 2" $ fact3 2 @?= 2
                            , testCase " fact3 7 -> 5040" $ fact3 7 @?= 5040]
                        , testGroup "factorial 4" $
                            [ testCase " fact4 1 -> 2" $ fact4 1 @?= 1
                            , testCase " fact4 5 -> 120" $ fact4 5 @?= 120]
                        , testGroup "factorial 52 " $
                            [ testCase " fact5 1 -> 2" $ fact5 1 @?= 1
                            , testCase " fact5 5 -> 120" $ fact5 5 @?= 120]
                        , testGroup "factorial 6" $
                            [ testCase " fact5 2 -> 2" $ fact6 2 @?= 2
                            , testCase " fact5 6 -> 720" $ fact6 6 @?= 720]
                        , testGroup "factorial 7" $
                            [ testCase " fact5 2 -> 2" $ fact7 2 @?= 2
                            , testCase " fact5 6 -> 720" $ fact7 6 @?= 720]
                        ]
                    ]

                ]

main = defaultMain pdfHaskellSuite