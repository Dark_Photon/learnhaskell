module Main where

import Test.Tasty
import Test.Tasty.HUnit

import Begin

beginSuite :: TestTree
beginSuite = testGroup " Beginning tests"
                [ testGroup "doubleMe" $
                    [ testCase " 2 = 4" $ doubleMe 2 @?= 4
                    , testCase " 15 = 30" $ doubleMe 15 @?= 30]
                , testGroup "doubleUs x y"$
                    [ testCase " (x=2)*2 + (y=4)*2 = 12" $ doubleUs 2 4 @?= 12
                    , testCase " (x=5)*2 + (y=12)*2 = 34"  $ doubleUs 5 12 @?= 34]
                , testGroup "doubleSmallNumber"$
                    [ testCase " 55 = 110" $ doubleSmallNumber 55 @?= 110
                    , testCase " 99 = 198"  $ doubleSmallNumber 99 @?= 198]
                , testGroup "doubleSmallNumber' = doubleSmallNumber + 1"$
                    [ testCase " 55 = 111" $ doubleSmallNumber' 55 @?= 111
                    , testCase " 99 = 199"  $ doubleSmallNumber' 99 @?= 199
                    , testCase " 44 = 89"  $ doubleSmallNumber' 44 @?= 89]
                ]

main = defaultMain beginSuite