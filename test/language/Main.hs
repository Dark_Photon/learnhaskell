module Main where

import Test.Tasty
import Test.Tasty.HUnit

import Utiles



languageSuite :: TestTree
languageSuite = testGroup " Language tests"
                [ testGroup "isVowel' -> i, u"$
                    [ testCase " a -> True" $ isVowel 'a' @?= True
                    , testCase " p -> False" $ isVowel 'p' @?= False
                    , testCase " i -> True" $ isVowel 'i' @?= True
                    , testCase " k -> False" $ isVowel 'k' @?= False]
                , testGroup "isOpenVowel' -> a, e, o" $
                    [ testCase " a -> True" $ isOpenVowel 'a'@?= True
                    , testCase " e -> True"  $ isOpenVowel 'e' @?= True
                    , testCase " u -> False"  $ isOpenVowel 'u' @?= False]
                , testGroup "isCloseVowel' -> i, u"$
                    [ testCase " i -> True" $ isCloseVowel 'i'@?= True
                    , testCase " u -> True"  $ isCloseVowel 'u' @?= True
                    , testCase " o -> False"  $ isCloseVowel 'o' @?= False]
                , testGroup "containOpenVowel"$
                    [ testCase " día = True" $ containOpenVowel   "dia" @?= True
                    , testCase " comedia = True" $ containOpenVowel "comedia" @?= True
                    , testCase " sir = False" $ containOpenVowel "sir" @?= False
                    , testCase " tu = False" $ containOpenVowel "tu" @?= False]
                , testGroup "containCloseVowel"$
                    [ testCase " dia -> True" $ containCloseVowel "dia"@?= True
                    , testCase " prado -> False"  $ containCloseVowel "prado" @?= False
                    , testCase " rio -> True"  $ containCloseVowel "rio" @?= True]
                , testGroup "splitSyllable"$
                    [ testCase " dia -> di, ia" $ splitSyllable "dia" @?= ["di", "ia"]
                    , testCase " prado -> pr, ra, ad, do" $ splitSyllable "prado" @?= ["pr", "ra", "ad", "do"]
                    , testCase " rio -> ri, io" $ splitSyllable "rio" @?= ["ri", "io"]]
                , testGroup "isDiptongo" $
                    [ testCase " dia -> True" $ isDiptongo "dia" @?= True
                    , testCase " prado -> False" $ isDiptongo "prado" @?= False
                    , testCase " rio -> True" $ isDiptongo "rio" @?= True
                    , testCase " comedia -> True" $ isDiptongo "comedia" @?= True
                    , testCase " tierra -> True" $ isDiptongo "tierra" @?= True
                    , testCase " piojo -> True" $ isDiptongo "piojo" @?= True
                    , testCase " riu -> True" $ isDiptongo "riu" @?= True
                    , testCase " fuego -> True" $ isDiptongo "fuego" @?= True
                    , testCase " paisaje -> True" $ isDiptongo "paisaje" @?= True
                    , testCase " peine -> True" $ isDiptongo "peine" @?= True
                    , testCase " presa -> False" $ isDiptongo "presa" @?= False
                    , testCase " saltar -> False" $ isDiptongo "saltar" @?= False]
                , testGroup "isDiptongo'" $
                    [ testCase " dia -> True" $ isDiptongo' "dia" @?= True
                    , testCase " prado -> False" $ isDiptongo' "prado" @?= False
                    , testCase " rio -> True" $ isDiptongo' "rio" @?= True
                    , testCase " comedia -> True" $ isDiptongo' "comedia" @?= True
                    , testCase " tierra -> True" $ isDiptongo' "tierra" @?= True
                    , testCase " piojo -> True" $ isDiptongo' "piojo" @?= True
                    , testCase " riu -> True" $ isDiptongo' "riu" @?= True
                    , testCase " fuego -> True" $ isDiptongo' "fuego" @?= True
                    , testCase " paisaje -> True" $ isDiptongo' "paisaje" @?= True
                    , testCase " peine -> True" $ isDiptongo' "peine" @?= True
                    , testCase " presa -> False" $ isDiptongo' "presa" @?= False
                    , testCase " saltar -> False" $ isDiptongo' "saltar" @?= False]
                 {-, testGroup "isTriptongo" $
                    [ testCase " Semiautomatico -> True" $ splitSyllableByNumber "Semiautomatico" 2@?= ["Se","mi","au","to","ma","ti","co"]
                    , testCase " Semiautomatico -> True" $ splitSyllableByNumber "Semiautomatico" 3@?= ["Sem","iau","tom","ati","co"]
                    , testCase " Semiautomatico -> True" $ splitSyllableByNumber "Semiautomatico" 4@?= ["Semi","auto","mati","co"]
                    , testCase " Parodiais -> True" $ splitSyllableByNumber "Parodiais" 4 @?= ["Paro","diai","s"]
                    , testCase " Limpiaunas -> True" $ splitSyllableByNumber "Limpiaunas" 2 @?= ["Li","mp","ia","un","as"]
                    , testCase " Semiautomatico -> True" $ splitSyllableByNumber "Semiautomatico" 1 @?= ["S","e","m","i","a","u","t","o","m","a","t","i","c","o"]
                    , testCase " piojo -> True" $ splitSyllableByNumber "piojo" 2 @?= ["pi","oj","o"]]
                 , testGroup "isTriptongo" $
                    [ testCase " Limpiaunas -> True" $ isTriptongo "Limpiaunas" @?= True
                    , testCase " Miau -> True" $ isTriptongo "Miau" @?= True
                    , testCase " Cambiais -> True" $ isTriptongo "Cambiais" @?= True
                    , testCase " Parodiais -> True" $ isTriptongo "Parodiais" @?= True
                    , testCase " Paraguay -> True" $ isTriptongo "Paraguay" @?= False
                    , testCase " Semiautomatico -> True" $ isTriptongo "Semiautomatico" @?= True
                    , testCase " Paraguas -> True" $ isTriptongo "Paraguas" @?= False
                    , testCase " Timpano -> True" $ isTriptongo "Timpano" @?= False
                    , testCase " piojo -> True" $ isTriptongo "piojo" @?= False]
                , testGroup "doubleSmallNumber"$
                    [ testCase " 55 = 110" $ doubleSmallNumber 55 @?= 110
                    , testCase " 99 = 198"  $ doubleSmallNumber 99 @?= 198]
                , testGroup "doubleSmallNumber' = doubleSmallNumber + 1"$
                    [ testCase " 55 = 111" $ doubleSmallNumber' 55 @?= 111
                    , testCase " 99 = 199"  $ doubleSmallNumber' 99 @?= 199
                    , testCase " 44 = 89"  $ doubleSmallNumber' 44 @?= 89] -}
                ]



main = defaultMain languageSuite