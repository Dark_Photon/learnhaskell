module Main where

import Test.Tasty
import Test.Tasty.HUnit

import BasicsElements

futureLearnSuite :: TestTree
futureLearnSuite = testGroup " Future Learn tests"
                [ testGroup "Week 1" $
                    [ testGroup "functions 1" $
                        [ testCase "Hello, Josh" $ hello "Josh" @?= "Hello, Josh"
                        , testCase "Hello, Elena" $ hello "Elena" @?= "Hello, Elena"
                        , testCase "Hello, Elena" $ hello "Elena" @?= "Hello, Elena"]
                    , testGroup "lists " $
                        [ testCase "['hola', 'Query', 'verdad']" $ lists @?= ["hola", "Query", "verdad"]
                        , testCase "['Hello', 'Elena', 'dicho']" $ ["Hello", "Elena", "dicho"] @?= ["Hello", "Elena", "dicho"]]
                    , testGroup "functions anonymous f -> x + y + z" $
                        [ testCase "f 1 2 3" $ f 1 2 3 @?= 6
                        , testCase "f 4 5 6" $ f 4 5 6 @?= 15]
                    , testGroup "[let in] roots x y z -> [root1,root2]" $
                        [ testCase "4 53 6 -> [-0.11419153,-0.11419153]" $ roots 4 53 6 @?= [-0.11419153,-0.11419153]
                        , testCase "3 33 67 -> [-2.686343,-2.686343]" $ roots 3 33 67 @?= [-2.686343,-2.686343]]
                    , testGroup "[if then else] max' x y " $
                        [ testCase "max' 5 7 -> 7" $ max' 5 7 @?= 7
                        , testCase "max' 33 67 -> 67" $ max' 33 67 @?= 67]
                    , testGroup "high-order-functions doubleList " $
                        [ testCase "doubleList 5-> [2,4,6,8,10]" $ doubleList 5 @?= [2,4,6,8,10]
                        , testCase "doubleList 3 -> [2,4,6]" $ doubleList 3@?= [2,4,6]]
                    ]
                ]

main = defaultMain futureLearnSuite