# LearnHaskell

El propósito general es aprender a trabajar con Haskell y Stack.

Poco a poco subiré ejemplos según vaya aprendiendo.


## Stack

### Instalar

Dejo el link donde explica cómo instalarlo tanto para linux como windows: [install Stack](https://docs.haskellstack.org/en/stable/README/)

### Compilar y ejecutar

Una vez instaldo Stack, dentro del repositorio clonado, ejecutamos lo siguiente:

- Compilar: este comando leera la configuración del proyecto y descargará lo necesario para que funcione la aplicación
```bash
learnHaskell$> stack build
```
- Ejecutar: este comando ejecutará el archivo `app/Main.hs` del proyecto, de momento apenas hay código pero tendría que salir en la terminal el mensaje "someFunc"
```bash
learnHaskell$> stack run
```
- Lanzar Tests:
    - Global: ejecutará todos los tests configurados en la aplicación (package.yaml)
    - Unit-Test : solo ejecutará el que elijamos
 
 Global:
 ```
 learnHaskell$> stack test
 ```
![](images/StackTests.jpg)
 
 Unit-Test:
 Ej: En este caso solo queremos ejecutar los tests "beginning" -> test/beginning/Main.hs
 ```
  learnHaskell$> stack build; stack runghc test/beginning/Main.hs  
 ```
![](images/UnitTestBegin.jpg)

Como vemos se ejecutan 2 comandos `stack build; stack runghc test/beginning/Main.hs`:
- stack build
- stack runghc test/beginning/Main.hs

Primero compila la aplicación con `stack build` y a continuación ejecuta con `stack runghc` el fichero test/beginning/Main.hs
    