module Introduction where

--conditional if-then-else
fact1 :: Int -> Int
fact1 n = if n == 0 then 1
                    else n * fact1 (n-1)

--Guards
fact2 :: Int -> Int
fact2 n
    | n == 0 = 1
    | otherwise = n * fact2 (n-1)


--Pattern Matching

fact3 :: Integer -> Integer
fact3 0 = 1
fact3 n = n * fact3 (n-1)


-- Restriccion del dominio mediante guardas
fact4 :: Integer -> Integer
fact4 n
    | n == 0 = 1
    | n >= 1 = n * fact4 (n-1)


-- Restricción del dominio mediante patrones ????
fact5 :: Integer -> Integer
fact5 0     = 1
fact5 n = n * fact5 (n-1)

-- Mediante Predefinidas
fact6 :: Integer -> Integer
fact6 n = product[1..n]


-- Mediante Plegado
fact7 :: Integer -> Integer
fact7 n = foldr (*) 1 [1..n]