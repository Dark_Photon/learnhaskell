module BasicsElements where


hello :: String -> String
hello name = "Hello, " ++ name

sq :: Int -> Int -> Int
sq x y = x*x + y*y


lists = ["hola", "Query", "verdad"]


--Funtions anonymous

f = \x y z -> x + y + z


-- let in

roots :: Float -> Float -> Float -> [Float]
roots a b c = let
                det2 = (b * b) - (4 * a * c)
                det = sqrt det2
                root1 = ((-b + det) / 2) / a
                root2 = ((-b + det) / 2) / a
              in root1:root2:[]


-- if then else

max' :: Int -> Int -> Int
max' x y = if x > y
            then x
            else y


-- high-order-functions
doubleList :: Int -> [Int]
doubleList n = map (\ x -> 2*x)[1..n]


data Color = Red | Blue | Yellow

color = Red

action = case color of
    Red -> "is Red"
    Blue -> "is Blue"
    Yellow -> "is Yellow"
