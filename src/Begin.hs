module Begin where

doubleMe :: Int -> Int
doubleMe n = n * 2

doubleUs :: Int -> Int -> Int
doubleUs x y = doubleMe x + doubleMe y


doubleSmallNumber :: Int -> Int
doubleSmallNumber number = if number > 100
                            then number
                            else number * 2

doubleSmallNumber' :: Int -> Int
doubleSmallNumber' x = (if x > 100 then x else x*2) + 1


doubleF :: Int -> Int
doubleF x = x * 2


doubleFTT :: Int -> Int
doubleFTT x = x * 2