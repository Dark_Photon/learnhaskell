module Utiles where

alphabet = ['a'..'z']
vowels = "aeiou"
consonants = [x | x <- alphabet, (notElem) x vowels ]

openVowelChar = "aeo"
closeVowelChar = "iu"

isVowel :: Char -> Bool
isVowel char = (elem) char vowels

isCloseVowel :: Char -> Bool
isCloseVowel 'i' = True
isCloseVowel 'u' = True
isCloseVowel x = False

isOpenVowel :: Char -> Bool
isOpenVowel 'a' = True
isOpenVowel 'e' = True
isOpenVowel 'o' = True
isOpenVowel x = False

containOpenVowel :: String -> Bool
containOpenVowel stringIO
    | (elem) (openVowelChar !! 0) stringIO = True
    | (elem) (openVowelChar !! 1) stringIO = True
    | (elem) (openVowelChar !! 2) stringIO = True
    | otherwise = False

containOpenVowel' stringI = (elem) True [ isOpenVowel x | x <- stringI]
containOpenVowel' :: String -> Bool


containCloseVowel :: String -> Bool
containCloseVowel stringIO
    | (elem) (closeVowelChar !! 0) stringIO = True
    | (elem) (closeVowelChar !! 1) stringIO = True
    | otherwise = False

containCloseVowel' :: String -> Bool
containCloseVowel' stringI = (elem) True [ isCloseVowel x | x <- stringI]


openVowel = ["a","e","o"]
closeVowel = ["i","u"]

combinationsOVCV = [ov ++ cv | ov <- openVowel, cv <- closeVowel]
combinationsCVCV = [closeVowel >>= id]

splitSyllable :: String -> [String]
splitSyllable cadena = init [ if x /= length(cadena)-1
                    then (cadena !! x):(cadena !! (x+1)):[]
                    else  [] | x <- [0..(length(cadena)-1)]]


isDiptongo :: String -> Bool
isDiptongo stringI
    | (elem) True dipOVCV || (elem) True dipCVCV = True
    | otherwise = False
    where splitSyll = splitSyllable stringI
          dipOVCV = [ True | x <- splitSyll, y <- combinationsOVCV, x == y || x == (reverse y) ]
          dipCVCV = [ True | x <- splitSyll, y <- combinationsCVCV, x == y || x == (reverse y) ]


-- triptongo --> VC-VO-VC

splitSyllableByNumber :: String -> Int -> [String]
splitSyllableByNumber string number = undefined


getUnitVocals :: String -> [String]
getUnitVocals string = [ if isVowel (string !! pos) && pos /= length(string)-1
                            then ((string !! pos):(string !! (pos+1)):[])
                            else  []
                        | pos <- [0..(length(string)-1)]]

isDiptongo' :: String -> Bool
isDiptongo' string
    | isOVCV || isCVCV = True
    | otherwise = False
    where listUnitVowels = (getUnitVocals string)
          isOVCV = (elem) True [ True | y <- combinationsOVCV, x <- listUnitVowels , x == y || x == (reverse y)]
          isCVCV = (elem) True [ True | y <- combinationsCVCV, x <- listUnitVowels , x == y || x == (reverse y)]

--dipOVCV = [ True | x <- splitSyll, y <- combinationsOVCV, x == y || x == (reverse y) ]
combinationsCVCOCV = [y++x++z | x <- openVowel, y <- closeVowel, z <- closeVowel]

get3UnitVocals :: String -> [String]
get3UnitVocals string = [ if isVowel (string !! pos) && pos /= length(string)-2
                            then ((string !! pos):(string !! (pos+1)):(string !! (pos+2)):[])
                            else  []
                        | pos <- [0..(length(string)-1)]]

get3UnitVocals' :: String -> [String]
get3UnitVocals' string = [ ((string !! pos):(string !! (pos+1)):(string !! (pos+2)):[]) | pos <- [0..(length(string)-1)] , isVowel (string !! pos) && pos /= (length(string)-2)]


